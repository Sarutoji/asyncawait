using NUnit.Framework;
using System;
using System.Threading.Tasks;

namespace AsyncAwait.Tests
{
    [TestFixture]
    public class TaskChainingTests
    {
        static readonly object[] Arrays =
        {
            new object[] { new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 }, 55 },
            new object[] { new int[] { 11, 22, 33, 44, 55, 66, 77, 88, 99, 100 }, 595 },
            new object[] { new int[] { 31, 32, 33, 34, 35, 36, 37, 38, 39, 40 }, 355 }
        };

        [TestCaseSource(nameof(Arrays))]
        public async Task MultiplyArrayTests(int[] array, int sum)
        {
            int multiplier = array[0];
            int[] result = await Program.MultiplyArray(array);
            multiplier = result[0] / multiplier;
            int sum2 = 0;
            for (int i = 0; i < result.Length; i++)
            {
                sum2 += result[i];
            }

            Assert.AreEqual((sum * multiplier), sum2);
        }

        [TestCaseSource(nameof(Arrays))]
        public void CalculateAvgOfArrayTests(int[] array, int sum)
        {
            double expectedAvg = sum / (double)10;
            double actualAvg = Program.CalculateAvgOfArray(array);
            Assert.AreEqual(expectedAvg, actualAvg);
        }

        [TestCase(new int[] { 55, 66, 77, 88, 99, 100, 11, 22, 33, 44 })]
        public void SortArrayAscendingTests(int[] array)
        {
            var expected = new int[] { 11, 22, 33, 44, 55, 66, 77, 88, 99, 100 };
            var actual = Program.SortArrayAscending(array);
            Assert.AreEqual(expected, actual);
        }

        [TestCase(null)]
        public void SortArrayAscendingThrowNullException(int[] array)
        {
            Assert.Throws<ArgumentNullException>(() => Program.SortArrayAscending(array));
        }

        [TestCase(null)]
        public void CalculateAvgOfArrayThrowNullException(int[] array)
        {
            Assert.Throws<ArgumentNullException>(() => Program.CalculateAvgOfArray(array));
        }
    }
}