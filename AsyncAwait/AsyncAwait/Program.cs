﻿using System;
using System.Threading.Tasks;

namespace AsyncAwait
{
#pragma warning disable S1118 // Utility classes should not have public constructors
    public class Program
#pragma warning restore S1118 // Utility classes should not have public constructors
    {
        static Random random = new Random();
        static void Main(string[] args)
        {
            GetAvgFromMultipliedArrayAsync();
        }

#pragma warning disable S3168 // "async" methods should not return "void"
        public static async void GetAvgFromMultipliedArrayAsync()
#pragma warning restore S3168 // "async" methods should not return "void"
        {
            var array = CreateArray();
            Task<int[]> getMulitipliedArray = MultiplyArray(array);

            SortArrayAscending(array);
            int[] result = await getMulitipliedArray;
            double avg = CalculateAvgOfArray(result);

            Console.WriteLine("Average of numbers in created and multiplied array: {0}", avg);
        }

        public static double CalculateAvgOfArray(int[] array)
        {
            if (array == null || Array.Empty<int>() == array)
            {
                throw new ArgumentNullException("array");
            }
            if (array.Length > 10 || array.Length < 10)
            {
                throw new ArgumentException("Array lenght must be 10");
            }
            for (int j = 0; j < array.Length; j++)
            {
                if (array[j] <= 0)
                {
                    throw new ArgumentException("Values in array must be higher than 0", nameof(array));
                }
            }

            double sum = 0;
            for (int i = 0; i < array.Length; i++)
            {
                sum += array[i];
            }

            return sum / 10;
        }

        public static int[] SortArrayAscending(int[] array)
        {
            if (array == null || Array.Empty<int>() == array)
            {
                throw new ArgumentNullException("array");
            }
            if (array.Length > 10 || array.Length < 10)
            {
                throw new ArgumentException("Array lenght must be 10");
            }
            Array.Sort(array);

            return array;
        }

        public static async Task<int[]> MultiplyArray(int[] array)
        {
            if (array == null || Array.Empty<int>() == array)
            {
                throw new ArgumentNullException("array");
            }
            if (array.Length > 10 || array.Length < 10)
            {
                throw new ArgumentException("Array lenght must be 10");
            }
            for (int j = 0; j < array.Length; j++)
            {
                if (array[j] <= 0)
                {
                    throw new ArgumentException("Values in array must be higher than 0", nameof(array));
                }
            }
            int multiplier = random.Next(1, 20);
            for (int i = 0; i < array.Length; i++)
            {
                await Task.Delay(0);
                array[i] *= multiplier;
            }

            return array;
        }

        private static int[] CreateArray()
        {
            int[] array = new int[10];
            for (int i = 0; i < 10; i++)
            {
                array[i] = random.Next(10, 100);
            }

            return array;
        }
    }
}
